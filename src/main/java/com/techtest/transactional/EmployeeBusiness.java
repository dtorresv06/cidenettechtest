package com.techtest.transactional;

import com.techtest.model.EmployeeModel;
import com.techtest.model.EmployeeRequestModel;
import com.techtest.model.LookupCriteria;
import com.techtest.model.OperationResult;
import io.reactivex.Flowable;
import io.reactivex.Single;

/**
 * @project cidenet
 * @Author Daniel Torres on 14/05/2022.
 **/

public interface EmployeeBusiness {

    Single<OperationResult> upsertEmployee(EmployeeRequestModel employee);

    Flowable<EmployeeModel> getEmployeesByCriteria(LookupCriteria criteria);

}

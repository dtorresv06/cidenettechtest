package com.techtest.transactional.implementations;

import com.techtest.mapper.EmployeeMapper;
import com.techtest.model.EmployeeModel;
import com.techtest.model.EmployeeRequestModel;
import com.techtest.model.LookupCriteria;
import com.techtest.model.OperationResult;
import com.techtest.repository.EmployeeRepository;
import com.techtest.transactional.EmployeeBusiness;
import com.techtest.utils.ResponseUtils;
import io.micronaut.core.annotation.Introspected;
import io.reactivex.Flowable;
import io.reactivex.Single;
import jakarta.inject.Inject;
import lombok.extern.slf4j.Slf4j;

/**
 * @project cidenet
 * @Author Daniel Torres on 14/05/2022.
 **/


@Slf4j
@Introspected
public class EmployeeBusinessImpl implements EmployeeBusiness {



    private final EmployeeRepository repository;
    private final EmployeeMapper mapper;

    @Inject
    public EmployeeBusinessImpl(EmployeeRepository repository, EmployeeMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    @Override
    public Single<OperationResult> upsertEmployee(EmployeeRequestModel employee) {

        return
        this.repository.upsertEmployee(mapper.toBusinessModel(employee))
                .doOnSubscribe(subs -> log.info(""))
                .doOnError(ResponseUtils.doOnError(log))
        ;
    }

    @Override
    public Flowable<EmployeeModel> getEmployeesByCriteria(LookupCriteria criteria) {
        return
                this.repository.getEmployeesByCriteria(criteria)
                        .doOnSubscribe(subs -> log.info(""))
                        .doOnError(ResponseUtils.doOnError(log))
                ;
    }
}

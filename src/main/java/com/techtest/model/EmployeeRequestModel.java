package com.techtest.model;


import javax.validation.constraints.*;

import io.micronaut.core.annotation.Introspected;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @project cidenet
 * @Author Daniel Torres on 14/05/2022.
 **/

@Getter
@Setter
@NoArgsConstructor

@Introspected
public class EmployeeRequestModel {

    public static final String NAMES_REGEX = "^([a-zA-Z ])+";

    private String id;

    @Pattern(regexp = NAMES_REGEX, message = "First surname should only contain characters from 'A' to 'Z'.")
    @Setter(AccessLevel.NONE)
    @Size(min = 1, max = 50, message = "First name should have at most 20 characters")
    private String firstSurname;

    @Pattern(regexp = NAMES_REGEX, message = "Second surname should only contain characters from 'A' to 'Z'.")
    @Setter(AccessLevel.NONE)
    @Size(min = 1, max = 50, message = "First name should have at most 20 characters")
    private String secondSurname;

    @Pattern(regexp = NAMES_REGEX, message = "First name should only contain characters from 'A' to 'Z'.")
    @Setter(AccessLevel.NONE)
    @Size(min = 1, max = 50, message = "First name should have at most 20 characters")
    private String firstName;

    @Pattern(regexp = NAMES_REGEX, message = "First surname should only contain characters from 'A' to 'Z'.")
    @Setter(AccessLevel.NONE)
    @Size(min = 1, max = 50, message = "Other names should have at most 50 characters")
    private String otherNames;

    private String idType;

    private String countryID;

    private String idNumber;

    private String email;

    public void setFirstSurname(String firstSurname){
        this.firstSurname = firstSurname.trim();
    }


}

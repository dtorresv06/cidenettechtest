package com.techtest.model;

import lombok.Builder;

/**
 * @project cidenet
 * @Author Daniel Torres on 14/05/2022.
 **/
@Builder
public class OperationResult {

    private String message;
    private Integer code;


}

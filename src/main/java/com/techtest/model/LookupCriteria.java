package com.techtest.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

/**
 * @project cidenet
 * @Author Daniel Torres on 14/05/2022.
 **/

@Getter
@Setter
@ToString
public class LookupCriteria extends EmployeeRequestModel{

    @Min(value = 1, message = "Minimum page is 1")
    private Integer page;

    @Max(value = 10, message = "Maximum page size is 10")
    @Min(value = 1, message = "Minimum page is 1")
    private Integer pageSize;


}

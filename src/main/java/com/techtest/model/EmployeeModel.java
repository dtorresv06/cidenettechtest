package com.techtest.model;

import io.micronaut.core.annotation.Introspected;
import lombok.Builder;
import lombok.ToString;

/**
 * @project cidenet
 * @Author Daniel Torres on 14/05/2022.
 **/
@Introspected
@Builder(toBuilder = true)
@ToString(callSuper = true)
public class EmployeeModel extends AuditModel{

    private String id;

    private String firstSurname;

    private String secondSurname;

    private String firstName;

    private String otherNames;

    private String idType;

    private String countryID;

    private String idNumber;

    private String email;

}

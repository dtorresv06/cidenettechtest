package com.techtest.mapper;

import com.techtest.model.EmployeeModel;
import com.techtest.model.EmployeeRequestModel;
//import com.techtest.repository.EmployeeEntity;
import com.techtest.repository.entities.EmployeeEntity;
import org.mapstruct.Mapper;

/**
 * @project cidenet
 * @Author Daniel Torres on 14/05/2022.
 **/
@Mapper(componentModel = "jsr330")
public interface EmployeeMapper {

    EmployeeModel toBusinessModel(EmployeeRequestModel request);

    EmployeeModel toBusinessModel(EmployeeEntity entity);

    EmployeeEntity toEntity(EmployeeModel model);

}

package com.techtest;

import io.micronaut.context.BeanContext;
import io.micronaut.context.event.StartupEvent;
import io.micronaut.http.server.HttpServerConfiguration;
import io.micronaut.runtime.Micronaut;
import io.micronaut.runtime.event.annotation.EventListener;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Application {

    public static void main(String[] args) {
        Micronaut.build(args)
                .run(Application.class, args).start();
    }

    @EventListener
    public void onStartup(StartupEvent event){
        final BeanContext beanContext = event.getSource();
        final HttpServerConfiguration httpServerConfiguration= beanContext.getBean(HttpServerConfiguration.class);
        log.info("App is running with context path ".concat(httpServerConfiguration.getContextPath()));
    }
}

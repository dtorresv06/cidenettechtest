package com.techtest.controllers;

import com.techtest.model.EmployeeModel;
import com.techtest.model.EmployeeRequestModel;
import com.techtest.model.LookupCriteria;
import com.techtest.model.OperationResult;
import com.techtest.transactional.EmployeeBusiness;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.reactivex.Flowable;
import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import jakarta.inject.Inject;
import lombok.extern.slf4j.Slf4j;
import static com.techtest.utils.ResponseUtils.doOnError;
import javax.validation.Valid;


/**
 * @project cidenet
 * @Author Daniel Torres on 14/05/2022.
 **/

@Controller(value = "/employees")
@Slf4j
public class EmployeeController {

    private final EmployeeBusiness employeeBusiness;

    @Inject
    public EmployeeController(EmployeeBusiness employeeBusiness) {
        this.employeeBusiness = employeeBusiness;
    }

    @Get(value = "/listBy" + "{?criteria}")
    Flowable<EmployeeModel> listBy(@Valid final LookupCriteria criteria){

        return employeeBusiness.getEmployeesByCriteria(criteria)
                .doOnSubscribe(subcription -> log.info("Retriving mployees"))
                .doOnError(doOnError(log));

    }

    @Post(value = "/upsert")
    Single<OperationResult> upsertEmployee(@Valid final EmployeeRequestModel employee){

        return employeeBusiness.upsertEmployee(employee)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe(subs -> log.info("Upserting employee with document {} ", employee.getIdNumber()))
                .doOnError(doOnError(log));
    }



}

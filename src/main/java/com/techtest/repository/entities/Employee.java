package com.techtest.repository.entities;

import com.techtest.repository.entities.AuditRepository;
import io.requery.*;

/**
 * @project cidenet
 * @Author Daniel Torres on 14/05/2022.
 **/

@Entity
@Table(name = "Employees")
public interface Employee extends AuditRepository {

    @Column(name = "id_employee")
    Integer getIdEmployee();

    @Column(name = "first_surname")
    String getFirstSurname();

    @Column(name = "second_surname")
    String getSecondSurname();

    @Column(name = "first_name")
    String getFirstName();

    @Column(name = "other_names")
    String getOtherNames();

    @Column(name = "email")
    String getEmail();

    @Column(name = "document_number")
    String getDocumentNumber();

    @ForeignKey(referencedColumn = "id_document")
    @Column(name = "id_document")
    Integer getIdDocument();

    @ForeignKey(referencedColumn = "id_country")
    @Column(name = "id_country")
    Integer getIdCountry();


}

package com.techtest.repository.entities;

import io.requery.Column;
import io.requery.Superclass;

import java.time.LocalDateTime;

/**
 * @project cidenet
 * @Author Daniel Torres on 16/05/2022.
 **/
@Superclass
public interface AuditRepository {

    @Column(name = "fecha_crea", value = "now()")
    LocalDateTime getFechaCrea();

    @Column(name = "fecha_modif")
    LocalDateTime getFechaModif();

}

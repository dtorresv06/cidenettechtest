package com.techtest.repository;

import com.techtest.model.EmployeeModel;
import com.techtest.model.EmployeeRequestModel;
import com.techtest.model.LookupCriteria;
import com.techtest.model.OperationResult;
import io.reactivex.Flowable;
import io.reactivex.Single;

/**
 * @project cidenet
 * @Author Daniel Torres on 15/05/2022.
 **/

public interface EmployeeRepository {

    Single<OperationResult> upsertEmployee(EmployeeModel employee);

    Flowable<EmployeeModel> getEmployeesByCriteria(LookupCriteria criteria);

}

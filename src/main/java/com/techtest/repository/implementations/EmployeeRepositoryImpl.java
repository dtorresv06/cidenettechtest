package com.techtest.repository.implementations;

import com.techtest.mapper.EmployeeMapper;
import com.techtest.model.EmployeeModel;
import com.techtest.model.EmployeeRequestModel;
import com.techtest.model.LookupCriteria;
import com.techtest.model.OperationResult;
import com.techtest.repository.EmployeeRepository;
import com.techtest.repository.entities.EmployeeEntity;
import io.micronaut.core.annotation.Introspected;
import io.reactivex.Flowable;
import io.reactivex.Single;
import io.requery.Persistable;
import io.requery.query.*;
import io.requery.reactivex.ReactiveEntityStore;
import io.requery.reactivex.ReactiveResult;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;


/**
 * @project cidenet
 * @Author Daniel Torres on 16/05/2022.
 **/
@Singleton
public class EmployeeRepositoryImpl implements EmployeeRepository {

    private final ReactiveEntityStore<Persistable> store;
    private final EmployeeMapper mapper;

    @Inject
    public EmployeeRepositoryImpl(ReactiveEntityStore<Persistable> store, EmployeeMapper mapper) {
        this.store = store;
        this.mapper = mapper;
    }

    @Override
    public Single<OperationResult> upsertEmployee(EmployeeModel employee) {

        return
        store.insert(mapper.toEntity(employee))
                .map(s -> OperationResult.builder().code(1).message("Sucess").build());
    }

    @Override
    public Flowable<EmployeeModel> getEmployeesByCriteria(LookupCriteria criteria) {

        Selection<ReactiveResult<EmployeeEntity>> select = store.select(EmployeeEntity.class);

        LogicalCondition<? extends Expression<Integer>, ?> documentTypeFilter = EmployeeEntity.ID_DOCUMENT.eq(Integer.parseInt(criteria.getIdType()));
        LogicalCondition<? extends LogicalCondition<? extends Expression<String>, ?>, Condition<?, ?>> documentFilter = EmployeeEntity.DOCUMENT_NUMBER.eq(criteria.getIdNumber()).
                and(documentTypeFilter);

        LogicalCondition<? extends Expression<String>, ?> firstNameFilter = EmployeeEntity.FIRST_NAME.eq(criteria.getFirstName());
        LogicalCondition<? extends Expression<String>, ?> firstSurnameFilter = EmployeeEntity.FIRST_SURNAME.eq(criteria.getFirstSurname());
        LogicalCondition<? extends Expression<String>, ?> secondSurnameFilter = EmployeeEntity.SECOND_SURNAME.eq(criteria.getSecondSurname());
        LogicalCondition<? extends Expression<String>, ?> otherNameFilter = EmployeeEntity.OTHER_NAMES.eq(criteria.getOtherNames());
        LogicalCondition<? extends Expression<String>, ?> emailFilter = EmployeeEntity.EMAIL.eq(criteria.getEmail());
        LogicalCondition<? extends Expression<Integer>, ?> countryFilter = EmployeeEntity.ID_COUNTRY.eq(Integer.parseInt(criteria.getCountryID()));

        WhereAndOr<ReactiveResult<EmployeeEntity>> where = select
                .where(documentFilter)
                        .or(firstNameFilter)
                        .or(firstSurnameFilter)
                        .or(secondSurnameFilter)
                        .or(otherNameFilter)
                        .or(documentTypeFilter)
                        .or(emailFilter)
                        .or(countryFilter)
                ;

        return where.get()
                .flowable()
                .map(mapper::toBusinessModel)
                ;

    }
}

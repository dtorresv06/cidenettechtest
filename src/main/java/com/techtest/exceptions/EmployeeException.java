package com.techtest.exceptions;

/**
 * @project cidenet
 * @Author Daniel Torres on 14/05/2022.
 **/
public class EmployeeException extends RuntimeException {

    private static final long serialVersionUID = -6202381788386413399L;

    private Throwable error;
    private final ResponseCode code;
    private final String message;


    public EmployeeException(ResponseCode code, String message) {
        super(message);
        this.code = code;
        this.message = message;
    }


}

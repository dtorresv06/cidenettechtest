package com.techtest.exceptions;

import lombok.Getter;

/**
 * @project cidenet
 * @Author Daniel Torres on 14/05/2022.
 **/
@Getter
public enum ResponseCode {

    SUCCESS("01"),
    ID_NUMBER_ALREADY_EXISTS("13");

    private String code;

    ResponseCode(String code) {
        this.code = code;
    }

}

package com.techtest;

import com.techtest.utils.StringUtils;
import io.micronaut.context.annotation.Context;
import io.micronaut.context.annotation.Factory;
import io.micronaut.context.annotation.Infrastructure;
import io.micronaut.context.annotation.Primary;
import io.requery.Persistable;
import io.requery.cache.EntityCacheBuilder;
import io.requery.meta.EntityModel;
import io.requery.meta.EntityModelBuilder;
import io.requery.reactivex.ReactiveEntityStore;
import io.requery.reactivex.ReactiveSupport;
import io.requery.sql.Configuration;
import io.requery.sql.ConfigurationBuilder;
import io.requery.sql.EntityDataStore;
import jakarta.inject.Inject;
import jakarta.inject.Named;

import javax.sql.DataSource;

/**
 * @project cidenet
 * @Author Daniel Torres on 15/05/2022.
 **/

@Factory
public class DataStoreConfig {

    private final DataSource store;


    @Inject
    public DataStoreConfig(@Named("employees") DataSource store) {
        this.store = store;
    }

    public static final EntityModel EMPLOYEESMODEL = new EntityModelBuilder("default")
            //.addType(AgenteEntity.$TYPE)
            .build();

    @Context
    @Primary
    @Infrastructure
    public ReactiveEntityStore<Persistable> entityDataStore() {

        EntityModel model = EMPLOYEESMODEL;

        Configuration configuration = new ConfigurationBuilder(store, model)
                //.useDefaultLogging()
                .setColumnTransformer(StringUtils::toUpperSnakeCase)
                .setEntityCache(new EntityCacheBuilder(model).build())
                .build();

        ReactiveEntityStore<Persistable> dataStore = ReactiveSupport
                .toReactiveStore(new EntityDataStore<>(configuration));
        return dataStore;
    }



}

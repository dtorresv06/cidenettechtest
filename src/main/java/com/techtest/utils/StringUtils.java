package com.techtest.utils;

/**
 * @project cidenet
 * @Author Daniel Torres on 15/05/2022.
 **/
public class StringUtils {

    public static String toUpperSnakeCase(String variableName) {
        StringBuilder builder = new StringBuilder();
        char[] nameChars = variableName.toCharArray();

        for(int i = 0; i < nameChars.length; ++i) {
            char ch = nameChars[i];
            if (i != 0 && Character.isUpperCase(ch)) {
                builder.append('_').append(ch);
            } else {
                builder.append(Character.toUpperCase(ch));
            }
        }

        return builder.toString().toLowerCase();
    }

}

package com.techtest.utils;

import io.reactivex.functions.Consumer;
import org.slf4j.Logger;

/**
 * @project cidenet
 * @Author Daniel Torres on 14/05/2022.
 **/
public class ResponseUtils {

    public static Consumer<? super Throwable> doOnError(
           final Logger log) {

        return error -> {
            log.error(error.getLocalizedMessage());
        };
    }

}
